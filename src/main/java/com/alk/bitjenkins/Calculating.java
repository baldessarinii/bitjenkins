package com.alk.bitjenkins;

/**
 * Created by Alexandr Nikitin on 23.06.16.
 */
public class Calculating {

    private int a,b;

    public Calculating(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int sum(){
        return a+b;
    }
}
